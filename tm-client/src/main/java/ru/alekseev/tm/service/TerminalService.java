package ru.alekseev.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.alekseev.tm.api.ITerminalService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public final class TerminalService implements ITerminalService {
    @NotNull
    public final static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    @Override
    @NotNull
    public final String getFromConsole() {
        String fromConsole = "";
        try {
            fromConsole = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return fromConsole;
    }
}
