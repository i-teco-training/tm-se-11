package ru.alekseev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.api.iendpoint.ISessionEndpoint;
import ru.alekseev.tm.api.iendpoint.Session;
import ru.alekseev.tm.command.system.AbstractCommand;

public final class UserLoginCommand extends AbstractCommand {
    @Override
    @NotNull
    public final String getName() {
        return "login";
    }

    @Override
    @NotNull
    public final String getDescription() {
        return "Login into Project Manager";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[LOGGING IN]");
        ISessionEndpoint sessionEndpoint = serviceLocator.getSessionEndpointService().getSessionEndpointPort();
        System.out.println("ENTER LOGIN");
        @NotNull final String login = serviceLocator.getTerminalService().getFromConsole();
        System.out.println("ENTER PASSWORD");
        @NotNull final String password = serviceLocator.getTerminalService().getFromConsole();
        @Nullable final Session newSession = sessionEndpoint.openSession(login, password);
        serviceLocator.setCurrentSession(newSession);
        System.out.println("[OK]");
    }

    @Override
    public final boolean isSecure() {
        return false;
    }
}
