package ru.alekseev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.api.iendpoint.ITaskEndpoint;
import ru.alekseev.tm.api.iendpoint.Session;
import ru.alekseev.tm.api.iendpoint.Task;
import ru.alekseev.tm.command.system.AbstractCommand;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public final class TaskSortCommand extends AbstractCommand {
    @Override
    @NotNull
    public final String getName() {
        return "sort-tasks";
    }

    @Override
    @NotNull
    public final String getDescription() {
        return "Show sorted list of all tasks";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[SORTED LIST OF ALL TASKS]");
        @NotNull final ITaskEndpoint taskEndpoint = serviceLocator.getTaskEndpointService().getTaskEndpointPort();
        @Nullable final Session currentSession = serviceLocator.getCurrentSession();
        if (currentSession == null) return;
        if (currentSession.getUserId() == null || currentSession.getUserId().isEmpty()) return;

        @Nullable final List<Task> listForSorting = taskEndpoint.findAllTasksByUserId(currentSession);
        if (listForSorting.size() == 0) {
            System.out.println("LIST OF TASKS IS EMPTY");
            return;
        }
        System.out.println("sort by?");
        System.out.println("1 - by date of creation, 2 - by start, 3 - by finish, 4 - by status");
        System.out.println("ENTER 1, 2, 3 or 4");
        @NotNull final String input = serviceLocator.getTerminalService().getFromConsole();
        switch (input) {
            case "1": Collections.sort(listForSorting, new CreatedOnComparator()); break;
            case "2": Collections.sort(listForSorting, new DateStartComparator()); break;
            case "3": Collections.sort(listForSorting, new DateFinishComparator()); break;
            case "4": Collections.sort(listForSorting, new StatusComparator()); break;
            default:
                System.out.println("Incorrect input. Try again.");
                return;
        }

        for (int i = 0; i < listForSorting.size(); i++) {
            System.out.print(i + 1);
            System.out.println(") name:" + listForSorting.get(i).getName()
                    + ", taskId: " + listForSorting.get(i).getId()
                    + ", date of creation: " + listForSorting.get(i).getCreatedOn()
                    + ", start date: " + listForSorting.get(i).getDateStart()
                    + ", finish date: " + listForSorting.get(i).getDateFinish()
                    + ", status: " + listForSorting.get(i).getStatus());
        }
    }

    @Override
    public final boolean isSecure() {
        return true;
    }

    public final class CreatedOnComparator implements Comparator<Task> {
        @Override
        public int compare(@NotNull final Task o1, @NotNull final Task o2) {
            return (int) (o1.getCreatedOn().toGregorianCalendar().getTime().getTime() - o2.getCreatedOn().toGregorianCalendar().getTime().getTime());
        }
    }

    public final class DateStartComparator implements Comparator<Task> {
        @Override
        public int compare(@NotNull final Task o1, @NotNull final Task o2) {
            if (o1.getDateStart() == null) return -1;
            if (o2.getDateStart() == null) return 1;
            return (int) (o1.getDateStart().toGregorianCalendar().getTime().getTime() - o2.getDateStart().toGregorianCalendar().getTime().getTime());
        }
    }

    public final class DateFinishComparator implements Comparator<Task> {
        @Override
        public int compare(@NotNull final Task o1, @NotNull final Task o2) {
            if (o1.getDateFinish() == null) return -1;
            if (o2.getDateFinish() == null) return 1;
            return (int) (o1.getDateFinish().toGregorianCalendar().getTime().getTime() - o2.getDateFinish().toGregorianCalendar().getTime().getTime());
        }
    }

    public final class StatusComparator implements Comparator<Task> {
        @Override
        public int compare(@NotNull final Task o1, @NotNull final Task o2) {
            return o1.getStatus().ordinal() - o2.getStatus().ordinal();
        }
    }
}
