package ru.alekseev.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.api.iendpoint.Domain;
import ru.alekseev.tm.api.iendpoint.Session;
import ru.alekseev.tm.command.system.AbstractCommand;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public final class DataJsonJaxbLoadCommand extends AbstractCommand {
    @Override
    @NotNull
    public final String getName() {
        return "json1-load";
    }

    @Override
    @NotNull
    public final String getDescription() {
        return "load from JAXB JSON";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[LOAD FROM JAXB JSON]");
        @Nullable final Session currentSession = serviceLocator.getCurrentSession();
        if (currentSession == null) return;
        if (currentSession.getUserId() == null || currentSession.getUserId().isEmpty()) return;

        System.out.println("ENTER PATH TO FILE");
        @NotNull final String filePath = serviceLocator.getTerminalService().getFromConsole();
        if (filePath.isEmpty()) return;

        @NotNull final File jsonFile = new File(filePath);
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

        @Nullable final Domain loadedDomain = (Domain) unmarshaller.unmarshal(jsonFile);
        if (loadedDomain == null) return;
        serviceLocator.getDomainEndpointService().getDomainEndpointPort().setDomain(currentSession, loadedDomain);
        System.out.println("[DATA LOADED]");
    }

    @Override
    public final boolean isSecure() {
        return true;
    }

    @Override
    public final boolean isForAdminOnly() {
        return true;
    }
}
