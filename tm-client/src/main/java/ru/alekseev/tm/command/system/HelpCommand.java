package ru.alekseev.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public final class HelpCommand extends AbstractCommand {
    @Override
    @NotNull
    public final String getName() {
        return "help";
    }

    @Override
    @NotNull
    public final String getDescription() {
        return "Show all commands";
    }

    @Override
    public final void execute() {
        @NotNull final List<AbstractCommand> allCommands = serviceLocator.getCommands();

        for (@Nullable final AbstractCommand abstractCommand : allCommands) {
            if (abstractCommand == null) continue;
            System.out.println(abstractCommand.getName() + " - " + abstractCommand.getDescription());
        }
    }

    @Override
    public final boolean isSecure() {
        return false;
    }
}
