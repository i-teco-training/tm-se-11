package ru.alekseev.tm.command.system;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.alekseev.tm.api.ServiceLocator;

@Setter
@Getter
@NoArgsConstructor
public abstract class AbstractCommand {

    protected ServiceLocator serviceLocator;

    public abstract String getName();

    public abstract String getDescription();

    public abstract void execute() throws Exception;

    public abstract boolean isSecure();

    public boolean isForAdminOnly() {
        return false;
    }
}
