package ru.alekseev.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.api.iendpoint.Domain;
import ru.alekseev.tm.api.iendpoint.Session;
import ru.alekseev.tm.command.system.AbstractCommand;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public final class DataXmlJaxbLoadCommand extends AbstractCommand {
    @Override
    @NotNull
    public final String getName() {
        return "xml1-load";
    }

    @Override
    @NotNull
    public final String getDescription() {
        return "load from JAXB XML";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[LOAD FROM JAXB XML]");
        Session currentSession = serviceLocator.getCurrentSession();
        if (currentSession == null) return;
        if (currentSession.getUserId() == null || currentSession.getUserId().isEmpty()) return;

        System.out.println("ENTER PATH TO FILE");
        @NotNull final String filePath = serviceLocator.getTerminalService().getFromConsole();
        if (filePath.isEmpty()) return;

        @NotNull final File xmlFile = new File(filePath);
        @Nullable final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @Nullable final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        @Nullable final Domain loadedDomain = (Domain) unmarshaller.unmarshal(xmlFile);
        if (loadedDomain == null) return;
        serviceLocator.getDomainEndpointService().getDomainEndpointPort().setDomain(currentSession, loadedDomain);
        System.out.println("[DATA LOADED]");
    }

    @Override
    public final boolean isSecure() {
        return true;
    }

    @Override
    public final boolean isForAdminOnly() {
        return true;
    }
}
