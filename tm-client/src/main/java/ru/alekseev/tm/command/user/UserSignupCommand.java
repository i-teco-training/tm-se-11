package ru.alekseev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.alekseev.tm.command.system.AbstractCommand;

public final class UserSignupCommand extends AbstractCommand {
    @Override
    @NotNull
    public final String getName() {
        return "signup";
    }

    @Override
    @NotNull
    public final String getDescription() {
        return "Create new user account";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[ADDING NEW USER ACCOUNT]");
        System.out.println("ENTER LOGIN");
        @NotNull final String login = serviceLocator.getTerminalService().getFromConsole();
        System.out.println("ENTER PASSWORD");
        @NotNull final String password = serviceLocator.getTerminalService().getFromConsole();
        serviceLocator.getUserEndpointService().getUserEndpointPort().addUserByLoginAndPassword(login, password);
        System.out.println("[OK]");
    }

    @Override
    public final boolean isSecure() {
        return false;
    }
}
