package ru.alekseev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.api.iendpoint.ITaskEndpoint;
import ru.alekseev.tm.api.iendpoint.Session;
import ru.alekseev.tm.api.iendpoint.Task;
import ru.alekseev.tm.command.system.AbstractCommand;

public final class TaskAddCommand extends AbstractCommand {
    @Override
    @NotNull
    public final String getName() {
        return "add-task";
    }

    @Override
    @NotNull
    public final String getDescription() {
        return "Add new task";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[ADDING OF NEW TASK]");
        @NotNull final ITaskEndpoint taskEndpoint =
                serviceLocator.getTaskEndpointService().getTaskEndpointPort();

        Session currentSession = serviceLocator.getCurrentSession();
        if (currentSession == null) return;
        if (currentSession.getUserId() == null || currentSession.getUserId().isEmpty()) return;

        System.out.println("TYPE \"1\" TO ATTACH NEW TASK TO EXISTING PROJECT (otherwise press \"ENTER\" key)");
        @NotNull final String userChoice = serviceLocator.getTerminalService().getFromConsole();
        String projectId = null;
        if ("1".equals(userChoice)) {
            System.out.println("ENTER PROJECT ID");
            projectId = serviceLocator.getTerminalService().getFromConsole();
        }
        System.out.println("ENTER TASK NAME");
        @NotNull final String taskName = serviceLocator.getTerminalService().getFromConsole();
        if (taskName.isEmpty()) {
            System.out.println("invalid input!");
            return;
        }

        taskEndpoint.addTaskByUserIdTaskName(currentSession, taskName);
        System.out.println("[OK]");
    }

    @Override
    public final boolean isSecure() {
        return true;
    }
}
