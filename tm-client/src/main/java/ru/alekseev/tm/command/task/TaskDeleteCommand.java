package ru.alekseev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.api.iendpoint.ITaskEndpoint;
import ru.alekseev.tm.api.iendpoint.Session;
import ru.alekseev.tm.command.system.AbstractCommand;

public final class TaskDeleteCommand extends AbstractCommand {
    @Override
    @NotNull
    public final String getName() {
        return "delete-task";
    }

    @Override
    @NotNull
    public final String getDescription() {
        return "Delete task by id";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[DELETING OF TASK]");
        @NotNull final ITaskEndpoint taskEndpoint =
                serviceLocator.getTaskEndpointService().getTaskEndpointPort();

        @Nullable final Session currentSession = serviceLocator.getCurrentSession();
        if (currentSession == null) return;
        if (currentSession.getUserId() == null || currentSession.getUserId().isEmpty()) return;

        System.out.println("ENTER TASK ID");
        @NotNull final String taskId = serviceLocator.getTerminalService().getFromConsole();
        if (taskId.isEmpty()) {
            System.out.println("invalid input!");
            return;
        }

        taskEndpoint.deleteTaskByTaskId(currentSession, taskId);
        System.out.println("[OK]");
    }

    @Override
    public final boolean isSecure() {
        return true;
    }
}
