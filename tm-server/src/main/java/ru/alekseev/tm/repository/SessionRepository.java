package ru.alekseev.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.alekseev.tm.api.irepository.ISessionRepository;
import ru.alekseev.tm.entity.Session;
import ru.alekseev.tm.util.SignatureUtil;

public class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {
    @Override
    @NotNull
    public Session open(String userId) {
        Session newSession = new Session();
        newSession.setUserId(userId);
        String signature = SignatureUtil.sign(newSession);
        newSession.setSignature(signature);
        map.put(newSession.getId(), newSession);
        return newSession;
    }
}
