package ru.alekseev.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.bootstrap.Bootstrap;

public final class Application {
    public static void main(@Nullable String[] args) throws Exception {
        @NotNull Bootstrap bootstrap = new Bootstrap();
        bootstrap.start();
    }
}
