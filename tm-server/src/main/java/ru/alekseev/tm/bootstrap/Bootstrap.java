package ru.alekseev.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import ru.alekseev.tm.api.ServiceLocator;
import ru.alekseev.tm.api.irepository.IProjectRepository;
import ru.alekseev.tm.api.irepository.ISessionRepository;
import ru.alekseev.tm.api.irepository.ITaskRepository;
import ru.alekseev.tm.api.irepository.IUserRepository;
import ru.alekseev.tm.api.iservice.*;
import ru.alekseev.tm.endpoint.*;
import ru.alekseev.tm.enumerated.RoleType;
import ru.alekseev.tm.repository.ProjectRepository;
import ru.alekseev.tm.repository.SessionRepository;
import ru.alekseev.tm.repository.TaskRepository;
import ru.alekseev.tm.repository.UserRepository;
import ru.alekseev.tm.service.*;
import ru.alekseev.tm.util.HashUtil;

import javax.xml.ws.Endpoint;

public final class Bootstrap implements ServiceLocator {
    @NotNull private final IProjectRepository projectRepository = new ProjectRepository();
    @NotNull private final IProjectService projectService = new ProjectService(projectRepository);
    @NotNull private final ITaskRepository taskRepository = new TaskRepository();
    @NotNull private final ITaskService taskService = new TaskService(taskRepository);
    @NotNull private final IUserRepository userRepository = new UserRepository();
    @NotNull private final IUserService userService = new UserService(userRepository);
    @NotNull private final ISessionRepository sessionRepository = new SessionRepository();
    @NotNull private final ISessionService sessionService = new SessionService(sessionRepository);

    @NotNull private final IDomainService domainService =
            new DomainService(projectRepository, taskRepository, userRepository);

    @Override
    @NotNull
    public final IProjectService getProjectService() {
        return projectService;
    }

    @Override
    @NotNull
    public final ITaskService getTaskService() {
        return taskService;
    }

    @Override
    @NotNull
    public final IUserService getUserService() {
        return userService;
    }

    @Override
    @NotNull
    public ISessionService getSessionService() {
        return sessionService;
    }

    @Override
    @NotNull
    public final IDomainService getDomainService() {
        return domainService;
    }

    @NotNull public final static String PROJECT_ENDPOINT_WSDL = "http://localhost:8080/ProjectEndpoint?wsdl";
    @NotNull public final static String SESSION_ENDPOINT_WSDL = "http://localhost:8080/SessionEndpoint?wsdl";
    @NotNull public final static String TASK_ENDPOINT_WSDL = "http://localhost:8080/TaskEndpoint?wsdl";
    @NotNull public final static String USER_ENDPOINT_WSDL = "http://localhost:8080/UserEndpoint?wsdl";
    @NotNull public final static String DOMAIN_ENDPOINT_WSDL = "http://localhost:8080/DomainEndpoint?wsdl";

    public final void start() {

        try {
            initUsers();
            initEndpoints();
        } catch (Exception e) {
            System.out.println("exception was thrown");
            e.printStackTrace();
        }
    }

    public final void initUsers() {
        this.userService.addByLoginPasswordUserRole("user1", HashUtil.getMd5("www"), RoleType.ADMIN);
        this.userService.addByLoginPasswordUserRole("user2", HashUtil.getMd5("www"), RoleType.USER);
    }

    public final void initEndpoints() {
        Endpoint.publish(PROJECT_ENDPOINT_WSDL, new ProjectEndpoint(this));
        Endpoint.publish(TASK_ENDPOINT_WSDL, new TaskEndpoint(this));
        Endpoint.publish(USER_ENDPOINT_WSDL, new UserEndpoint(this));
        Endpoint.publish(SESSION_ENDPOINT_WSDL, new SessionEndpoint(this));
        Endpoint.publish(DOMAIN_ENDPOINT_WSDL, new DomainEndpoint(this));
    }
}