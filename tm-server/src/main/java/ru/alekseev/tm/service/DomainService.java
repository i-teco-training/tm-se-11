package ru.alekseev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.api.iservice.IDomainService;
import ru.alekseev.tm.api.irepository.IProjectRepository;
import ru.alekseev.tm.api.irepository.ITaskRepository;
import ru.alekseev.tm.api.irepository.IUserRepository;
import ru.alekseev.tm.dto.Domain;
import ru.alekseev.tm.entity.Project;
import ru.alekseev.tm.entity.Task;
import ru.alekseev.tm.entity.User;

import java.util.List;

public final class DomainService implements IDomainService {

    private final IProjectRepository projectRepository;
    private final ITaskRepository taskRepository;
    private final IUserRepository userRepository;

    public DomainService(
            @NotNull IProjectRepository projectRepository,
            @NotNull ITaskRepository taskRepository,
            @NotNull IUserRepository userRepository
    ) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
        this.userRepository = userRepository;
    }

    @Override
    @NotNull
    public final Domain getDomain() {
        @NotNull final List<Project> projects = projectRepository.findAll(); //здесь можно final?
        @NotNull final List<Task> tasks = taskRepository.findAll();
        @NotNull final List<User> users = userRepository.findAll();
        @NotNull final Domain domain = new Domain();
        domain.setProjects(projects);
        domain.setTasks(tasks);
        domain.setUsers(users);
        return domain;
    }

    @Override
    public final void setDomain(@Nullable final Domain domain) {
        projectRepository.clear();
        projectRepository.addAll(domain.getProjects());
        taskRepository.clear();
        taskRepository.addAll(domain.getTasks());
        userRepository.clear();
        userRepository.addAll(domain.getUsers());
    }
}
