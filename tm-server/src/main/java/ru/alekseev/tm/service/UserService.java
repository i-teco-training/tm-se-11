package ru.alekseev.tm.service;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.api.irepository.IUserRepository;
import ru.alekseev.tm.api.iservice.IUserService;
import ru.alekseev.tm.enumerated.RoleType;
import ru.alekseev.tm.entity.User;

import java.util.List;

@Getter
@Setter
public class UserService extends AbstractService<User> implements IUserService {
    private final IUserRepository userRepository;
    public UserService(@NotNull IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    @Nullable
    public final User findOne(@NotNull final String id) {
        return userRepository.findOne(id);
    }

    @Override
    @NotNull
    public final List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    @Nullable
    public final User findOneByLoginAndPassword(@NotNull final String login, @NotNull final String passwordHashcode) {
        if (login.isEmpty() || passwordHashcode.isEmpty()) return null; //ok?
        return userRepository.findOneByLoginAndPasswordHashcode(login,passwordHashcode);
    }

    @Override
    public final void add(@NotNull final User user) {
        userRepository.persist(user);
    }

    @Override
    public void addByLoginPasswordUserRole(
            @NotNull final String login,
            @NotNull final String passwordHashcode,
            @NotNull final RoleType roleType
    ) {
        if (login.isEmpty() || passwordHashcode.isEmpty() || roleType.toString().isEmpty()) return;
        userRepository.addByLoginPasswordUserRole(login, passwordHashcode, roleType);
    }

    @Override
    public final void addByLoginAndPassword(
            @NotNull final String login,
            @NotNull final String passwordHashcode
    ) {
        userRepository.addByLoginAndPassword(login,passwordHashcode);
    }

    @Override
    public final void update(@NotNull final User user) {
        userRepository.merge(user);
    }

    @Override
    public void updateUserPassword(@NotNull final String userId, @NotNull final String passwordHashcode) {
        userRepository.updateUserPassword(userId, passwordHashcode);
    }

    @Override
    public void updateUserByRole(String userId, RoleType roleType) {
        userRepository.updateUserByRole(userId, roleType);
    }

    @Override
    public final void delete(@NotNull final String id) {
        userRepository.delete(id);
    }

    @Override
    public final void deleteByLoginAndPassword(@NotNull final String login, @NotNull final String passwordHashcode) {
        if (login.isEmpty() || passwordHashcode.isEmpty()) return;
        userRepository.deleteByLoginAndPassword(login, passwordHashcode);
    }

    @Override
    public final void clear() {
        userRepository.clear();
    }
}
