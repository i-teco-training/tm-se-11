package ru.alekseev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.api.irepository.ISessionRepository;
import ru.alekseev.tm.api.iservice.ISessionService;
import ru.alekseev.tm.entity.Session;
import ru.alekseev.tm.util.SignatureUtil;

import java.util.List;

public final class SessionService extends AbstractService<Session> implements ISessionService {
    private ISessionRepository sessionRepository;

    public SessionService(ISessionRepository sessionRepository) {
        this.sessionRepository = sessionRepository;
    }

    @Override
    public final void add(@NotNull final Session entity) {
        sessionRepository.persist(entity);
    }

    @Override
    @Nullable
     public final List<Session> findAll() {
        return sessionRepository.findAll();
    }

    @Override
    public final void update(@NotNull final Session entity) {
        sessionRepository.merge(entity);
    }

    @Override
    public final void delete(@NotNull final String id) {
        sessionRepository.delete(id);
    }

    @Override
    public final void clear() {
        sessionRepository.clear();
    }

    @Override
    public final Session open(@NotNull final String userId) {
        return sessionRepository.open(userId);
    }

    @Override
    public boolean isValid(@NotNull final Session session) {
        if (session == null) return false;
        if (session.getSignature() == null || session.getSignature().isEmpty()) return false;
        if (session.getUserId() == null || session.getUserId().isEmpty()) return false;
        if (session.getTimestamp() == null) return false;
        @NotNull final String sourceSignature = session.getSignature();
        session.setSignature(null);
        @NotNull final String targetSignature = SignatureUtil.sign(session);
        //if (sessionRepository.findOne(session.getId()) == null) return false;
        return sourceSignature.equals(targetSignature);
    }
}
