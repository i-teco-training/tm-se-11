package ru.alekseev.tm.enumerated;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@Getter
@NoArgsConstructor
public enum Status {
    PLANNED("planned"),
    INPROGRESS("in progress"),
    READY("ready");

    private String displayName;

    Status(final String displayName) {
        this.displayName = displayName;
    }

    @Override
    @Nullable
    public final String toString() {
        return this.displayName;
    }
}
