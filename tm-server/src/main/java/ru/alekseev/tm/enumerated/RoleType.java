package ru.alekseev.tm.enumerated;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@Getter
@NoArgsConstructor
public enum RoleType {
    USER("user"),
    ADMIN("administrator");

    private String displayName;

    RoleType(final String displayName) {
        this.displayName = displayName;
    }

    @Override
    @Nullable
    public final String toString() {
        return this.displayName;
    }
}
