package ru.alekseev.tm.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.entity.Project;
import ru.alekseev.tm.entity.Task;
import ru.alekseev.tm.entity.User;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@XmlRootElement
public class Domain implements Serializable {
    @Nullable private List<User> users;
    @Nullable private List<Project> projects;
    @Nullable private List<Task> tasks;
}
