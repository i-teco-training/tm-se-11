package ru.alekseev.tm.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.api.ServiceLocator;
import ru.alekseev.tm.api.iendpoint.IDomainEndpoint;
import ru.alekseev.tm.dto.Domain;
import ru.alekseev.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Setter
@Getter
@NoArgsConstructor
@WebService(endpointInterface = "ru.alekseev.tm.api.iendpoint.IDomainEndpoint")
public class DomainEndpoint implements IDomainEndpoint {
    private ServiceLocator serviceLocator;

    public DomainEndpoint(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    @Nullable
    @WebMethod
    public Domain getDomain(@WebParam @NotNull final Session session) {
        if (!serviceLocator.getSessionService().isValid(session)) {
            serviceLocator.getSessionService().delete(session.getId());
            return null;
        }
        return serviceLocator.getDomainService().getDomain();
    }

    @Override
    @WebMethod
    public void setDomain(
            @WebParam @NotNull final Session session,
            @WebParam @NotNull final Domain domain
    ) {
        if (!serviceLocator.getSessionService().isValid(session)) {
            serviceLocator.getSessionService().delete(session.getId());
            return;
        }
        serviceLocator.getDomainService().setDomain(domain);
    }
}
