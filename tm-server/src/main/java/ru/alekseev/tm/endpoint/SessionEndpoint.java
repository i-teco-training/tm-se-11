package ru.alekseev.tm.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.alekseev.tm.api.ServiceLocator;
import ru.alekseev.tm.api.iendpoint.ISessionEndpoint;
import ru.alekseev.tm.entity.Session;
import ru.alekseev.tm.exception.AccessForbiddenException;
import ru.alekseev.tm.util.HashUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Setter
@Getter
@NoArgsConstructor
@WebService(endpointInterface = "ru.alekseev.tm.api.iendpoint.ISessionEndpoint")
public class SessionEndpoint implements ISessionEndpoint {
    private ServiceLocator serviceLocator;

    public SessionEndpoint(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    @NotNull
    @WebMethod
    public Session openSession(
            @WebParam @NotNull final String login,
            @WebParam @NotNull final String password
    ) {
        String passwordHashcode = HashUtil.getMd5(password);
        String userId = serviceLocator.getUserService().findOneByLoginAndPassword(login, passwordHashcode).getId();
        return serviceLocator.getSessionService().open(userId);
    }

    @Override
    @WebMethod
    public boolean isSessionValid(@WebParam @NotNull final Session session) {
        return serviceLocator.getSessionService().isValid(session);
    }

    @Override
    @WebMethod
    public void closeSession(@WebParam @NotNull final String sessionId) {
        serviceLocator.getSessionService().delete(sessionId);
    }
}
