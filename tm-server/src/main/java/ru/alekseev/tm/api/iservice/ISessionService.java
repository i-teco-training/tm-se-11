package ru.alekseev.tm.api.iservice;

import ru.alekseev.tm.entity.Session;
import ru.alekseev.tm.exception.AccessForbiddenException;

public interface ISessionService extends IService<Session>{

    Session open(String userId);

    boolean isValid(Session session);// throws AccessForbiddenException;
}
