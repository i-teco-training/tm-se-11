package ru.alekseev.tm.api.iendpoint;

import org.jetbrains.annotations.NotNull;
import ru.alekseev.tm.entity.Session;
import ru.alekseev.tm.entity.Task;

import javax.jws.WebService;
import java.util.List;

@WebService
public interface ITaskEndpoint {

    Task findOneTaskByUserIdAndTaskId(String userId, String taskId);

    List<Task> findAllTasks();

    List<Task> findAllTasksByUserId(Session session);

    void addTask(Task entity);

    void addTaskByUserIdTaskName(Session session, String taskName);

    void updateTask(Task entity);

    void updateTaskByTaskIdTaskName(Session session, String taskId, String taskName);

    void deleteTask(String id);

    void deleteTaskByTaskId(Session session, String taskId);

    void clearTasks();

    void clearTasksByProjectId(String projectId);

    void clearTasksByUserIdProjectId(Session session, String projectId);
}
