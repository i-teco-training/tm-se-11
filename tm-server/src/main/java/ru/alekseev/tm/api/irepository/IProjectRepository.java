package ru.alekseev.tm.api.irepository;

import ru.alekseev.tm.entity.Project;

import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    Project findOneByUserIdAndProjectId(String userId, String projectId);

    void addProjectByUserIdProjectName(String userId, String projectName);

    List<Project> findAllByUserId(String userId);

    void updateByUserIdProjectIdProjectName(String userId, String projectId, String projectName);

    void deleteByUserId(String userId);

    void deleteProjectByProjectId(String userId, String projectId);
}
