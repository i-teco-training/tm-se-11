package ru.alekseev.tm.api;

import ru.alekseev.tm.api.iservice.*;

public interface ServiceLocator {

    IProjectService getProjectService();

    ITaskService getTaskService();

    IUserService getUserService();

    ISessionService getSessionService();

    //IService getService();
//
    IDomainService getDomainService();
}
