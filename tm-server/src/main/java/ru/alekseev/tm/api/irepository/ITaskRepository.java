package ru.alekseev.tm.api.irepository;

import org.jetbrains.annotations.NotNull;
import ru.alekseev.tm.entity.Task;

import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    Task findOneByUserIdAndTaskId(String userId, String taskId);

    List<Task> findAllByUserId(String userId);

    void addTaskByUserIdTaskName(String userId, String taskName);

    void clearByProjectId(String projectId);

    void clearByUserIdAndProjectId(String userId, String projectId);

    void deleteByUserIdAndTaskId(String userId, String taskId);

    void updateByNewData(String userId, String taskId, String name);
}
