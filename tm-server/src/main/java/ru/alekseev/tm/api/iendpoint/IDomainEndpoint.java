package ru.alekseev.tm.api.iendpoint;

import ru.alekseev.tm.dto.Domain;
import ru.alekseev.tm.entity.Session;

import javax.jws.WebService;

@WebService
public interface IDomainEndpoint {

    Domain getDomain(Session session);

    void setDomain(Session session, Domain domain);
}
