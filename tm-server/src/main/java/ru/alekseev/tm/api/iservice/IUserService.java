package ru.alekseev.tm.api.iservice;

import org.jetbrains.annotations.NotNull;
import ru.alekseev.tm.entity.User;
import ru.alekseev.tm.enumerated.RoleType;

public interface IUserService extends IService<User> {

    User findOne(@NotNull final String id);

    User findOneByLoginAndPassword(String login, String passwordHashcode);

    void addByLoginAndPassword(String login, String passwordHashcode);

    void addByLoginPasswordUserRole(String login, String passwordHashcode, RoleType roleType);

    void updateUserPassword(String userId, String passwordHashcode);

    void updateUserByRole(String userId, RoleType roleType);

    void deleteByLoginAndPassword(String login, String passwordHashcode);
}
