package ru.alekseev.tm.api.iendpoint;

import ru.alekseev.tm.entity.Session;

import javax.jws.WebService;

@WebService
public interface ISessionEndpoint {/////наполнить!!!!!

    Session openSession(String login, String passwordHashcode);

    boolean isSessionValid(Session session) ;

    void closeSession(String sessionId);
}
