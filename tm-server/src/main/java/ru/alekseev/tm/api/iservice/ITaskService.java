package ru.alekseev.tm.api.iservice;

import ru.alekseev.tm.entity.Task;

import java.util.List;

public interface ITaskService extends IService<Task>{

    Task findOneByUserIdAndTaskId(String userId, String taskId);

    List<Task> findAllByUserId(String userId);

    void addTaskByUserIdTaskName(String userId, String taskName);

    void updateByNewData(String userId, String taskId, String name);

    void deleteByUserIdAndTaskId(String userId, String taskId);

    void clearByProjectId(String projectId);

    void clearByUserIdProjectId(String userId, String projectId);
}